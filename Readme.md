# Markcompass [![Build Status](https://travis-ci.org/wapidstyle/markcompass.svg?branch=master)](https://travis-ci.org/wapidstyle/markcompass)
Markcompass is a edition of Markdown intended for creating websites. Written in Java, the
project was meant for my Octopus project, which, at the time of this project's creation was 
called Jigsaw 2. Markcompass intends to add JavaScript and CSS functionality to Markdown, 
which is why it was created.
## Installation
There is no current way to install Markcompass, however soon there are plans for a gem, as
well as a way to use Choco and Homebrew. If I ever get around to making Doorstep, then you 
can use that too.
