package wapidstyle.markup;

public class Interpreter {
	public String input;
	public String rawinput;
	public String output;
	public static String lastoutput;
	
	/**
	 * The state of if the inputed code is a header.
	 */
	public boolean isHeader;
	/**
	 * The level of header the input is.
	 */
	public int headerLevel;
	/**
	 * The state of if the input is a blockquote.
	 * In both Markdown and Markcompass, the code for blockquote would be:
	 * <code> > The quote </code>
	 * displaying as:
	 * <blockquote>The quote</blockquote>
	 */
	public boolean isQuote;
	/**
	 * The state of if the input is code.
	 */
	public boolean isCode;
	/**
	 * The language that the code specifies.
	 */
	public String language;
	/**
	 * Interprets the Markcompass into HTML for display.
	 * @since #1
	 * @param <code>in</code> - The input to be interpreted.
	 * @return The HTML code for the input.
	 */
	public String interpretToHtml(String in){
		input = in;
		//switch(Header.getHeaderLevel(input)){
		//case 1:{
		//	return "<h1>" + rawinput + "</h1>";
		//}
		//}
		//	isHeader = true;
		//	headerLevel = getHeader(input);
		//} else {
		//	isHeader = false;
		//}
		//if(isQuote(input) == true){
		//	isQuote = true;
		//} else {
		//	isQuote = false;
		//}
		//if(isCode(input) == true){
		//	isCode = true;
		//	getCodeLang(input);
		//}
		//if(isCode = true){
		//	output = "" + "<code>" + rawinput;
		//}
		return "";
	}
	public boolean isHeader(String in){
		if(in.startsWith("#") == true){
			return true;
		} else {
			return false;
		}
	}
	public int getHeader(String in){
		if(isHeader(in) == true){
			if(in.startsWith("##") == true){
				if(in.startsWith("###") == true){
					if(in.startsWith("####") == true){
						if(in.startsWith("#####") == true){
							if(in.startsWith("######") == true){
								return 6; // Level 6 Header.
							} else {
								return 5; // Level 5 Header.
							}
						} else {
							return 4; // Level 4 Header.
						}
					} else {
						return 3; // Is a Level 3 Header.
					}
				} return 2; // Is a Level 2 Header.
			} else {
				return 1; // Is a Level 1 Header.
			}
		} else {
			return 0; // Is not a header.
		}
	}
	public boolean isQuote(String in){
		if(in.startsWith(">")){
			return true;
		} else {
			return false;
		}
	}
	public boolean isCode(String in){
		if(in.startsWith("`")){
			return true;
		} else {
			return false;
		}
	}
	public String getCodeLang(String in){
		if(in.startsWith("java", 3)){
			return "text/java";
		} else {
			if(in.startsWith("javascript", 3)){
				return "javascript";
			}
		}
		// TODO Finish this.
		return "";
	}
}
