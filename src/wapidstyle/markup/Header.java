package wapidstyle.markup;

public class Header{
	
	public static String input;
	
	public static int getHeaderLevel(String in) throws InterruptedException{
		input = in;
		
		switch(getLevel(input)){
		case ONE:{
			return 1;
		}
		case TWO:{
			return 2;
		}
		case THREE:{
			return 3;
		}
		case FOUR:{
			return 4;
		}
		case FIVE:{
			return 5;
		}
		case SIX:{
			return 6;
		}
		case NONE:{
			return 0;
		}
		}
		return 0;
	}
	private static HeaderLevel getLevel(String in){
		if(isLevel1(in) == BooleanValue.TRUE){
			return HeaderLevel.ONE;
		}
		if(isLevel2(in) == BooleanValue.TRUE){
			return HeaderLevel.TWO;
		}
		if(isLevel3(in) == BooleanValue.TRUE){
			return HeaderLevel.THREE;
		}
		if(isLevel4(in) == BooleanValue.TRUE){
			return HeaderLevel.FOUR;
		}
		if(isLevel5(in) == BooleanValue.TRUE){
			return HeaderLevel.FIVE;
		}
		if(isLevel6(in) == BooleanValue.TRUE){
			return HeaderLevel.SIX;
		}
		return HeaderLevel.NONE;
	}
	public static BooleanValue isLevel1(String in){
		if(in.startsWith("#") && !(isLevel2(in) == BooleanValue.TRUE)){
			return BooleanValue.TRUE;
		} else {
			return BooleanValue.FALSE;
		}
	}
	public static BooleanValue isLevel2(String in){
		if(in.startsWith("##") && !(isLevel3(in) == BooleanValue.TRUE)){
			return BooleanValue.TRUE;
		}
		return BooleanValue.FALSE;
	}
	public static BooleanValue isLevel3(String in){
		if(in.startsWith("###") && !(isLevel4(in) == BooleanValue.TRUE)){
			return BooleanValue.TRUE;
		}
		return BooleanValue.FALSE;
	}
	public static BooleanValue isLevel4(String in){
		if(in.startsWith("####") && !(isLevel5(in) == BooleanValue.TRUE)){
			return BooleanValue.TRUE;
		}
		return BooleanValue.FALSE;
	}
	public static BooleanValue isLevel5(String in){
		if(in.startsWith("#####") && !(isLevel6(in) == BooleanValue.TRUE)){
			return BooleanValue.TRUE;
		}
		return BooleanValue.FALSE;
	}
	public static BooleanValue isLevel6(String in){
		if(in.startsWith("######")){
			return BooleanValue.TRUE;
		}
		return BooleanValue.FALSE;
	}
}
